<?php

namespace Drupal\background_slider\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileUrlGeneratorInterface;

/**
 * Provides a 'sliderblock' block.
 *
 * @Block(
 *   id = "slider_block",
 *   admin_label = @Translation("Slider Block"),
 *   category = @Translation("Custom Slider Block")
 * )
 */
class SliderBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The form builder.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected $formBuilder;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The storage handler class for files.
   *
   * @var \Drupal\file\FileStorage
   */
  private $fileStorage;

  /**
   * The file URL generator.
   *
   * @var \Drupal\Core\File\FileUrlGeneratorInterface
   */
  protected $fileUrlGenerate;

  /**
   * Constructs a sliderblock object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   Module handler.
   * @param \Drupal\Core\Form\FormBuilderInterface $form_builder
   *   The form builder service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity
   *   The Entity type manager service.
   * @param \Drupal\Core\File\FileUrlGeneratorInterface $file_url_generate
   *   The file URL generator.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ModuleHandlerInterface $moduleHandler, FormBuilderInterface $form_builder, ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entity, FileUrlGeneratorInterface $file_url_generate) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->moduleHandler = $moduleHandler;
    $this->formBuilder = $form_builder;
    $this->configFactory = $config_factory;
    $this->fileStorage = $entity->getStorage('file');
    $this->fileUrlGenerate = $file_url_generate;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('module_handler'),
      $container->get('form_builder'),
      $container->get('config.factory'),
      $container->get('entity_type.manager'),
      $container->get('file_url_generator')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    global $base_url;
    $module_path = $base_url . '/' . $this->moduleHandler->getModule('background_slider')->getPath();

    $form = $this->formBuilder->getForm('Drupal\background_slider\Form\SliderForm');

    $slider_content = [];
    $config = $this->configFactory->get('background_slider.setting');
    $no_of_slides = $config->get('no_of_slides');
    $slider_type = $config->get('slider_type');

    for ($i = 1; $i <= $no_of_slides; $i++) {

      $slider_image_fid = $config->get("slide{$i}_image");
      $slider_url = "";
      if (!empty($slider_image_fid)) {
        $slider_image_obj = $this->fileStorage->load($slider_image_fid[0]);
        $uri = $slider_image_obj->getFileUri();
        $slider_url = $this->fileUrlGenerate->generateAbsoluteString($uri);
      }

      $slider_video_fid = $config->get("slide{$i}_video");
      $video_path = "";
      if (!empty($slider_video_fid)) {
        $slider_video_obj = $this->fileStorage->load($slider_video_fid[0]);
        $uri = $slider_video_obj->getFileUri();
        $video_path = $this->fileUrlGenerate->generateAbsoluteString($uri);
      }

      if ($slider_type == 'field_image') {
        $slider_content[$i] = '<img src="' . $slider_url . '" alt="Slider Images" class="d-block" style="width:100%" />';
      }
      else {
        $slider_content[$i] = '<video class ="d-block" autoplay loop muted style="width:100%; height:auto;">
																<source src="' . $video_path . '" type="video/mp4" />
															</video>';
      }
    }

    return [
      '#theme' => 'block__slider_block',
      '#form' => $form,
      'slider_content' => $slider_content,
      '#module_path' => $module_path,
      '#attached' => [
        'library' => [
          'background_slider/slider-style',
        ],
      ],
    ];
  }

}
