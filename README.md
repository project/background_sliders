The Background Sliders module is a slider.
This module allows to add slide with 2 types slider in 
option first is image and second is video.

INSTALLATION
The installation of this module is like other Drupal modules.


If your site is managed via Composer,
use Composer to download the background sliders module running
composer require "drupal/background_sliders".
Otherwise copy/upload the background sliders
module to the modules directory of your Drupal installation.


Enable the 'Background Sliders' module and got to path.
(/admin/config/filter_term/vocab)



CONFIGURATION

1. Click Configure button from admin/modules 
and go to configuration form page
2. Here you get Number of slides and slider type option
3. enter the number of slides required and select the 
slider type which ypu want & click Apply.
4. Select the images and videos which you choose in
slider type option and save this form
5. Now go to the block layout structure admin/structure/block
to find out your Slider Block and place on region where you 
want to show that slider.


MAINTAINERS
Current maintainers for Drupal 10:

Arti (arti_parmar) - https://www.drupal.org/u/arti_parmar
